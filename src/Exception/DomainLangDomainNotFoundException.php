<?php

namespace Drupal\domain_lang\Exception;

/**
 * Class DomainLangDomainNotFoundException.
 */
class DomainLangDomainNotFoundException extends \Exception {}
