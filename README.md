CONTENTS OF THIS FILE
---------------------
 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Use case

INTRODUCTION
------------
Domain Lang extends Domain Access module and add ability setup
Language detection and selection for every available sub-domain.

REQUIREMENTS
------------
* Domain Access (with enabled Domain Configuration module).

INSTALLATION
------------
Install as you would normally install a contributed Drupal module. Visit
https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules
for further information.

CONFIGURATION
-------------
* Enable Domain Lang module
* Go to available domains page: /admin/config/domain
* Add at list one domain (if wasn't added yet)
* In domain "Operations" dropdown click "Language Detection and Selection" link
* Setup language detection and selection
* Click "Save settings" button

USE CASE
--------
After enable module on domain edit page will appears new tab
"Language Detect & Select". On this tab You can setup language
detection and selection for edited domain.
